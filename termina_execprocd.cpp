#include "definitions.h"

main(int argc, char *argv[])
{
 int id_fila_kill;
 mensagem_t msg;
 // processo_t processo;
  
  printf("Bem-vindo ao termina_escalonador\n");

  /* pega ID da fila de mensagens */
  if ((id_fila_kill = msgget(MSG_KEY_KILL, IPC_CREAT|0x1B6)) < 0)
   {
     printf("erro na criacao da fila de matar processo\n");
     exit(1);
   }

  msg.tipo = KILL_ESCALONADOR;
  msg.id = 0;
  
  if((msgsnd(id_fila_kill, &msg, sizeof(msg)-sizeof(long), 0)) == 0)
  {
    printf("mensagem de matar escalonador enviada\n");
  }
  return 0;
}