#include "definitions.h"

main(int argc, char *argv[])
{
 int id, id_fila_kill;
 mensagem_t msg;
 // processo_t processo;
  
  printf("Bem-vindo ao cancela_proc\n");
  // printf("args = %d\n", argc);

  if (argc < 2)
  {
    printf("numero de argumentos errado\n");
    exit(1);
  }

  /* pega ID da fila de mensagens */
   if ((id_fila_kill = msgget(MSG_KEY_KILL, IPC_CREAT|0x1B6)) < 0)
   {
     printf("erro na criacao da fila de matar processo\n");
     exit(1);
   }

  msg.tipo = KILL_PROCESSO;
  msg.id = atoi(argv[1]);
  
  printf("antes do envio da mensagem\n");
  if((msgsnd(id_fila_kill, &msg, sizeof(msg)-sizeof(long), 0)) == 0)
  {
    printf("mensagem de matar processo %ld enviada\n", msg.id);
  }
  return 0;
}