/*
* ESCALONADOR DE PROCESSOS
* Data: 14/11/2014
* Lucas Lo Ami Alvino Silva
*
* Este arquivo simula o funcionamento do escalonador de processos. Ele recebe via mensagem
* todos os processos que devem ser executados bem como os comandos de cancelar um processo
* específico e cancelar a execução do escalonador
* 
* Há uma simulação do quantum do escalonador em milisegundos
* 
* Estatísticas dos processos e do escalonador são printadas em tela assim que estes são 
* finalizados
*/



#include "definitions.h"
#include <sys/wait.h>
#include <list>

using namespace std;  // Faz com quem nao precise usar std::

list<processo_t> tabela_processo;
list<long> fila_alta_prioridade, fila_baixa_prioridade, fila_media_prioridade;

void  escreve_processo_tela(processo_t processo)
{
  processo.tempoFim = time(0);  
  // printf("endID = %d\n", id_final_processo);
  printf("\n");
  printf("Dados do processo que terminou\n");
  printf("identificador = %ld\n", processo.identificador);
  printf("trocas de contexto = %d\n", processo.contadorContexto);
  // printf("tempo de execucao sem quantum = %f\n", difftime(processo.tempoFim, processo.tempoInicio));
  printf("wallclock time = %f\n", difftime(processo.tempoFim, processo.tempoInicio));
  printf("----------------------------------\n");
}

void atualiza_tabela_processos(processo_t processo)
{
  for(list<processo_t>::iterator it = tabela_processo.begin(); it != tabela_processo.end();)
  {
    if (it->identificador == processo.identificador)
    {
      it->pid = processo.pid;
      it->prioridade = processo.prioridade;
      it->status = processo.status;
      it->contadorPrioridade = processo.contadorPrioridade;
      it->contadorContexto = processo.contadorContexto;
      // printf("atualiza ====> pid = %d\n", (*it).pid);
      // printf("atualiza ====> it id = %ld\n", (*it).identificador);
      // printf("atualiza ====> id do proc = %ld\n", processo.identificador);
      break;
    }
    else ++it;
  }
}

processo_t  busca_e_remove_processo(long id)
{
  // printf("id=%ld\n", id);
  processo_t processo;
  processo.pid = 0;
  for(list<processo_t>::iterator it = tabela_processo.begin(); it != tabela_processo.end();)
  {
    // printf("id=%ld ----- pid=%d\n", it->identificador, it->pid);
    if (it->identificador == id)
    {
      processo.pid = it->pid;
      processo.contadorContexto = it->contadorContexto;
      processo.tempoInicio = it->tempoInicio;
      processo.tempoFim = it->tempoFim;
      processo.identificador = it->identificador;
      it = tabela_processo.erase(it);
      break;
    }
    else ++it;
  }
  return processo;
}

processo_t busca_processo()
{
  long id;
  processo_t processo;
  if (!fila_alta_prioridade.empty())
  {
    id = fila_alta_prioridade.front();
    fila_alta_prioridade.pop_front();
    // processo = procura_tabela_processos(id)
  }
  else if (!fila_media_prioridade.empty()) 
  {
    id = fila_media_prioridade.front();
    fila_media_prioridade.pop_front();
  }
  else 
  {
    id = fila_baixa_prioridade.front();
    fila_baixa_prioridade.pop_front();
  }

  for(list<processo_t>::iterator it = tabela_processo.begin(); it != tabela_processo.end();)
  {
    if (it->identificador == id)
    {
      processo.prioridade = it->prioridade;
      processo.pid = it->pid;
      processo.identificador = it->identificador;
      strcpy(processo.nomePrograma, it->nomePrograma);
      processo.status = it->status;
      processo.contadorPrioridade = it->contadorPrioridade;
      processo.contadorContexto = it->contadorContexto;
      processo.tempoInicio = it->tempoInicio;
      processo.tempoFim = it->tempoFim;
      break;
    }
    else ++it;
  }

  return processo;
}

processo_t executa_processo(processo_t processo)
{
  int pid;
  // printf("ANTES processo.pid=%d ---- pid = %d ----- status=%d\n", processo.pid, pid, processo.status);
  if (processo.status == STATUS_ESPERA)
  {
    pid = processo.pid;
    processo.status = STATUS_EXEC;
    kill(processo.pid, SIGCONT);
  } else 
  {
    if ((pid = fork()) < 0)
    {
      printf("Erro ao duplicar o processo usando fork()\n");
      exit(1);
    }

    processo.pid = pid;
    // printf("processo.pid=%d ---- pid = %d antes\n", processo.pid, pid);
    if (pid == 0)
    {
      char* argv[MAX_ARGUMENTOS+1];
      int i;
      for (i = 0; i < processo.contadorArgumento; i++)
      {
        argv[i] = processo.argumentosPrograma[i];
      }
      argv[i] = (char *) 0;

      
      processo.status = STATUS_EXEC;

      if (execv(processo.nomePrograma, argv) < 0)
      {
        printf("exec deu erro\n");
        exit(1);
      }
      // exit(EXIT_SUCCESS);
    }
  }
  // printf("processo.pid=%d ---- pid = %d ----- status=%d\n", processo.pid, pid, processo.status);
  return processo;
}

void mata_processo(long id) 
{

  int status;
  
  processo_t processo = busca_e_remove_processo(id);
  
  if (processo.pid > 0)
  {
    if (kill(processo.pid, SIGTERM) < 0)
    {
      printf("erro ao matar o processo = %s\n", strerror(errno));
      return;
    }
  } else
  {
    printf("pid nao existe\n");
    return;
  }
  waitpid(processo.pid,&status,0);

  escreve_processo_tela(processo);

  // int i, endID;
  // for (i = 0; i < 10; i++)
  // {
  //   sleep(1);
  //   endID = waitpid(pid, &status, WNOHANG|WUNTRACED);
  //   printf("endID = %d\n", endID);
  // }

  // printf("Child exit code: %d\n", WEXITSTATUS(status));
  // if (WIFEXITED(status))
  //   printf("Processo filho finalizou corretamente");
  // else if (WIFSIGNALED(status))
  //   printf("processo com id=%ld e pid=%d morto\n", id, pid);
  // else if (WIFSTOPPED(status))
  //    printf("Child process has stopped.n");

}


int filas_empty() 
{
  return fila_alta_prioridade.empty() && fila_media_prioridade.empty() && fila_baixa_prioridade.empty();
}


main()
{
  int id_fila_ready, pid, status, id_final_processo;
  processo_t processo;
  mensagem_t msg;
  int id_fila_kill;
  int total_processos_exec, total_processos_cancel, total_trocas_contexto;

  total_trocas_contexto = 0;
  total_processos_cancel = 0;
  total_processos_exec = 0;

  /* cria */
  if ((id_fila_ready = msgget(MSG_KEY_SUBMIT_PROCESS, IPC_CREAT|0x1B6)) < 0)
  {
   printf("erro na criacao da fila\n");
   exit(1);
  }

    /* cria */
  if ((id_fila_kill = msgget(MSG_KEY_KILL, IPC_CREAT|0x1B6)) < 0)
  {
   printf("erro na criacao da fila de matar processo\n");
   exit(1);
  }

  while (1) {
    
    if (msgrcv(id_fila_ready, &processo, sizeof(processo)-sizeof(long), PRIORIDADE_ALTA, IPC_NOWAIT) > 0) 
    {
      tabela_processo.push_back(processo);
      fila_alta_prioridade.push_back(processo.identificador);
      total_processos_exec += 1;
    } else if (msgrcv(id_fila_ready, &processo, sizeof(processo)-sizeof(long), PRIORIDADE_MEDIA, IPC_NOWAIT) > 0) 
    {
      tabela_processo.push_back(processo);
      fila_media_prioridade.push_back(processo.identificador);
      total_processos_exec += 1;
    } else if (msgrcv(id_fila_ready, &processo, sizeof(processo)-sizeof(long), PRIORIDADE_BAIXA, IPC_NOWAIT) > 0) 
    {
      tabela_processo.push_back(processo);
      fila_baixa_prioridade.push_back(processo.identificador);
      total_processos_exec += 1;
    }

    if (msgrcv(id_fila_kill, &msg, sizeof(msg)-sizeof(long), KILL_PROCESSO, IPC_NOWAIT) > 0)
    {
      mata_processo(msg.id);
      total_processos_cancel += 1;
    }
    if (msgrcv(id_fila_kill, &msg, sizeof(msg)-sizeof(long), KILL_ESCALONADOR, IPC_NOWAIT) > 0)
    {
      break;
    }


    if (!filas_empty())
    {
      processo = executa_processo(busca_processo());
      

      /*
      *
      *
      * SECAO COM O QUANTUM
      *
      */

      usleep(1000*QUANTUM);

      //CHECA SE O PROCESSO TERMINOU SOZINHO - SEM UM KILL
      id_final_processo = waitpid(processo.pid, &status, WNOHANG|WUNTRACED);
      if (id_final_processo > 0)
      {
        escreve_processo_tela(busca_e_remove_processo(processo.identificador));
        continue;
      }

      // ESTATISTICA DO ESCALONADOR
      total_trocas_contexto += 1;
      // FIM DA ESTATISTICA DO ESCALONADOR

      kill(processo.pid, SIGTSTP);
      // printf("processo.pid = %d\n", processo.pid);
      processo.status = STATUS_ESPERA;
      processo.contadorContexto += 1;
      processo.contadorPrioridade += 1;

      //DECIDE EM QUAL FILA DE PRIORIDADE O PROCESSO VAI SER INSERIDO
      if (processo.prioridade == PRIORIDADE_ALTA)
      {
        // processo.contadorPrioridade += 1;
        if (processo.contadorPrioridade == NVEZES_ALTA)
        {
          processo.contadorPrioridade = 0;
          processo.prioridade = PRIORIDADE_MEDIA;
          fila_media_prioridade.push_back(processo.identificador);  
        } else
            fila_alta_prioridade.push_back(processo.identificador);
      }
      else if (processo.prioridade == PRIORIDADE_MEDIA)
      {
        // processo.contadorPrioridade += 1;
        if (processo.contadorPrioridade == NVEZES_MEDIA)
        {
          processo.contadorPrioridade = 0;
          processo.prioridade = PRIORIDADE_BAIXA;
          fila_baixa_prioridade.push_back(processo.identificador);
        } else
          fila_media_prioridade.push_back(processo.identificador);
      }
      else 
      {
        // processo.contadorPrioridade += 1;
        if (processo.contadorPrioridade == NVEZES_BAIXA)
        {
          processo.contadorPrioridade = 0;
          processo.prioridade = PRIORIDADE_ALTA;
          fila_alta_prioridade.push_back(processo.identificador);
        } else
          fila_baixa_prioridade.push_back(processo.identificador);
      }

      
      atualiza_tabela_processos(processo);
      
    }

  }

  printf("----------------------------------------\n");
  printf("ESTATISTICAS DO ESCALONADOR\n");
  printf("total de processos executados = %d \n", total_processos_exec);
  printf("total de processos cancelados = %d\n", total_processos_cancel);
  printf("total de trocas de contexto = %d\n", total_trocas_contexto);
  printf("----------------------------------------\n");
  kill(-getpid(), SIGTERM);
  kill(getpid(), SIGTERM);
  return 0;

}