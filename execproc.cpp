
/*
  - TODO: Pegar argumentos do processo, caso haja
*/

#include "definitions.h"

main(int argc, char *argv[])
{
 int i, id_fila_ready;
 struct info_processo processo;
  
  printf("Bem-vindo ao execproc\n");
  // printf("args = %d\n", argc);

  if (argc < 3)
  {
    printf("numero de argumentos errado\n");
    exit(1);
  }

  /* pega ID da fila de mensagens */
   if ((id_fila_ready = msgget(MSG_KEY_SUBMIT_PROCESS, IPC_CREAT|0x1B6)) < 0)
   {
     printf("erro na criacao da fila\n");
     exit(1);
   }

  if (( i = fork()) < 0) {
    printf("Erro ao duplicar o processo usando fork() no arquivo %s\n", argv[0]);
    exit(1);
  }

  if (i == 0) 
  {
    strcpy(processo.nomePrograma, argv[1]);
    processo.prioridade = atoi(argv[2]);
    processo.contadorArgumento = 0;
    for (i = 3; i < argc; i++)
    {
      strcpy(processo.argumentosPrograma[i-3], argv[i]);
      processo.contadorArgumento += 1;
      // printf("arg[%d] = %s\n", i, argv[i]);
    }

    processo.identificador = getpid();
    processo.status = STATUS_NOEXEC;
    processo.contadorPrioridade = 0;
    processo.contadorContexto = 0;
    processo.tempoInicio = time(0);
    processo.pid = 0;
  
    printf("prioridade=%ld\n", processo.prioridade);
    printf("nome do programa = %s\n", processo.nomePrograma);
    printf("identificador = %ld\n", processo.identificador);
  
    if((msgsnd(id_fila_ready, &processo, sizeof(processo)-sizeof(long), 0)) == 0)
    {
      printf("mensagem enviada\n");
    }
  }
  return 0;
}