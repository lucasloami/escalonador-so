#ifndef _DEFINITIONS_H
#define _DEFINITIONS_H

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <time.h>

#define MSG_KEY_SUBMIT_PROCESS 0x124
#define MSG_KEY_KILL 0x127
#define KILL_PROCESSO 1
#define KILL_ESCALONADOR 2
#define PRIORIDADE_ALTA 1
#define PRIORIDADE_MEDIA 2
#define PRIORIDADE_BAIXA 3
#define STATUS_EXEC 1
#define STATUS_ESPERA 2
#define STATUS_MORTO 3
#define STATUS_NOEXEC 4
#define NVEZES_ALTA 1
#define NVEZES_MEDIA 2
#define NVEZES_BAIXA 4
#define QUANTUM 250 // microsegundos
#define MAX_ARGUMENTOS 5



typedef struct info_processo {
  long prioridade;
  int pid; // PID real do processo
  long identificador; //PID para a troca de mensagens
  char nomePrograma[50];
  char argumentosPrograma[MAX_ARGUMENTOS+1][20];
  int contadorArgumento;
  int status;
  int contadorPrioridade;
  int contadorContexto;
  time_t tempoInicio;
  time_t tempoFim;
  // char paramsPrograma[10][10];
  // int prioridade;
} processo_t;

typedef struct info_mensagem {
  long tipo; // 
  long id;
} mensagem_t;

#endif